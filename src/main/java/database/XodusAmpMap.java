package database;

import amp.AmpConstants;
import amp.AmpLogging;
import amp.Amplet;
import amp.exceptions.AmpException;
import amp.serialization.IAmpAmpletSerializable;
import jetbrains.exodus.ArrayByteIterable;
import jetbrains.exodus.ByteIterable;
import jetbrains.exodus.env.*;
import logging.IAmpexLogger;

import java.io.UnsupportedEncodingException;

public class XodusAmpMap
{
    //region Member Fields
    private String fileName = null;
    private Environment env = null;
    private Store store = null;
    private EnvironmentConfig config = null;
    //endregion

    //region Constructor
    public XodusAmpMap(String _filename)
    {
        this(_filename, 50);
    }

    public XodusAmpMap(String _fileName, int _maxMemoryUsagePercent)
    {
        if(_maxMemoryUsagePercent < 1)
        {
            _maxMemoryUsagePercent = 1;
        }

        if(_maxMemoryUsagePercent > 100)
        {
            _maxMemoryUsagePercent = 100;
        }

        fileName = _fileName;

        config = new EnvironmentConfig();
        config.setMemoryUsagePercentage(_maxMemoryUsagePercent);

        env = Environments.newInstance(_fileName, config);
        store = env.computeInTransaction(txn -> env.openStore(_fileName + "store", StoreConfig.WITHOUT_DUPLICATES_WITH_PREFIXING, txn));
    }
    //endregion

    //region Put
    public boolean put(byte[] _key, Amplet _amplet)
    {
        if (_key != null && _amplet != null && _amplet.isValidAmplet())
        {
            try
            {
                final ByteIterable convertedKey = new ArrayByteIterable(_key.clone());
                final ByteIterable convertedValue = new ArrayByteIterable(_amplet.serializeToBytes());

                env.executeInTransaction(txn -> store.put(txn, convertedKey, convertedValue));
                return true;
            } catch (AmpException e)
            {
                IAmpexLogger logger = AmpLogging.getLogger();
                if (logger != null)
                {
                    logger.error("", e);
                }
            }
        }
        return false;
    }

    public boolean put(byte[] _key, IAmpAmpletSerializable _serializableObject)
    {
        return put(_key, _serializableObject.serializeToAmplet());
    }

    public boolean put(String _key, Amplet _amplet)
    {
        return put(_key.getBytes(AmpConstants.UTF8), _amplet);
    }

    public boolean put(String _key, IAmpAmpletSerializable _serializableObject)
    {
        try
        {
            return put(_key.getBytes("UTF-8"), _serializableObject);
        } catch (UnsupportedEncodingException e)
        {
            IAmpexLogger logger = AmpLogging.getLogger();
            if (logger != null)
            {
                logger.error("", e);
            }
            return false;
        }
    }

    public boolean putBytes(byte[] _key, byte[] _input)
    {
        if (_key != null && _input != null)
        {
            try
            {
                final ByteIterable convertedKey = new ArrayByteIterable(_key.clone());
                final ByteIterable convertedValue = new ArrayByteIterable(_input.clone());

                env.executeInTransaction(txn -> store.put(txn, convertedKey, convertedValue));
                return true;
            } catch (Exception e)
            {
                IAmpexLogger logger = AmpLogging.getLogger();
                if (logger != null)
                {
                    logger.error("", e);
                }
            }
        }
        return false;
    }

    public boolean putBytes(String _key, byte[] _input)
    {
        return putBytes(_key.getBytes(AmpConstants.UTF8), _input);
    }
    //endregion

    //region Get
    public Amplet get(byte[] _key) throws Exception
    {
        if (_key != null)
        {
            final ByteIterable convertedKey = new ArrayByteIterable(_key.clone());

            ByteIterable output = env.computeInReadonlyTransaction(txn -> store.get(txn, convertedKey));

            if (output == null)
            {
                return null;
            }

            return Amplet.create(output.getBytesUnsafe());
        }
        return null;
    }

    public Amplet get(String _key) throws Exception
    {
        return get(_key.getBytes(AmpConstants.UTF8));
    }

    public byte[] getBytes(byte[] _key)
    {
        if (_key != null)
        {
            final ByteIterable convertedKey = new ArrayByteIterable(_key.clone());

            ByteIterable output = env.computeInReadonlyTransaction(txn -> store.get(txn, convertedKey));

            if (output == null)
            {
                return null;
            }

            return output.getBytesUnsafe().clone();
        }
        return null;
    }

    public byte[] getBytes(String _key)
    {
        return getBytes(_key.getBytes(AmpConstants.UTF8));
    }
    //endregion

    //region Remove
    public void remove(byte[] _key)
    {
        final ByteIterable convertedKey = new ArrayByteIterable(_key.clone());

        env.executeInTransaction(txn -> store.delete(txn, convertedKey));
    }

    public boolean remove(String _key)
    {
        remove(_key.getBytes(AmpConstants.UTF8));
        return true;
    }
    //endregion

    public void close()
    {
        env.close();
    }

    public void clear()
    {
        env.clear();
    }

    public synchronized void clearSafe()
    {
        env.clear();
        env.close();
        env = Environments.newInstance(fileName, config);
        store = env.computeInTransaction(txn -> env.openStore(fileName + "store", StoreConfig.WITHOUT_DUPLICATES_WITH_PREFIXING, txn));
    }

    //region GC
    public void gc()
    {
        env.gc();
    }

    public void suspendGC()
    {
        env.suspendGC();
    }

    public void resumeGC()
    {
        env.resumeGC();
    }
    //endregion
}

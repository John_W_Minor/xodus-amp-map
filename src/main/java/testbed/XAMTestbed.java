package testbed;

import database.XodusAmpMap;

import java.util.Random;

/**
 * Xodus Amp Map Library created by John Minor.
 */
public class XAMTestbed
{
    public static void main(String[] args)
    {
        XodusAmpMap xam = new XodusAmpMap("test directory", 10);

        int incrementer = 1;

        Random random = new Random();

        while (true)
        {
            String key = "BLAGOBLAG " + incrementer;
            incrementer++;

            byte[] randomJunk = new byte[random.nextInt(1000000) + 1];
            random.nextBytes(randomJunk);

            xam.putBytes(key, randomJunk);

            xam.getBytes(key);
        }
    }
}
